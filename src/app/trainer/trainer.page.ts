import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Pokemon } from "../models/pokemon.model";
import { PokemonService } from "../services/pokemon.service";

@Component({
    selector: 'app-trainer',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit{
    constructor(private router: Router,
        private readonly pokemonService: PokemonService){}

    pokemonz: String[] = []

    public goToLoginPage(){
        this.router.navigate(['login'])
    }

    //
    public goToCataloguePage(){
        this.router.navigate(['catalogue'])
    }

    // Expose variables to the template
    get pokemon(): Pokemon[]{
        return this.pokemonService.pokemon()
    }


    // ngOnInit is called when component is created
    // We fetch pokemon when component is created
    ngOnInit(): void{
        const currentUser = localStorage.getItem('currentUser')

        // If no user is logged in, redirect to login page
        if (currentUser === null) {
            this.router.navigate(['login'])
        }

        const data = localStorage.getItem('users')
        let users = []
        let userData = []

        if (data !== null) {
            users = JSON.parse(data)
            
            // Get current user and add pokemon to list of collected pokemon
            for (let i = 0; i < users.length; i++) {
                if (users[i].username === currentUser) {
                    // Only add if pokemon is not already registered
                    userData = users[i]
                }
            }
        }
        this.pokemonz = userData.pokemon
        console.log(userData);
        

        
    }
}