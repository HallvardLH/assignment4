import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.css']
})
export class LoginPage{
    constructor(private router: Router){}

    // Click on login button in form in login page
    public onSubmit(form: NgForm): void{
        // Get user input for username
        const username = form.value.username

        // Set current user to user that was typed in
        localStorage.setItem('currentUser', username)

        const data = localStorage.getItem('users')
        let users = []

        if (data !== null) {
            users = JSON.parse(data)

            // Create list of existing usernames so that we dont duplicate users
            const existingUsernames = []
            for (let i = 0; i < users.length; i++) {
                existingUsernames.push(users[i].username)
            }
            
            // Change to catalogue page if username is already registered
            if (existingUsernames.indexOf(username) > -1){
                this.router.navigate(['catalogue'])
                return
            }

            const newObj = {
                username: username,
                pokemon: []
            }
            users.push(newObj)
        }
        else{ // Create new object if there is no instance
            users = [{
                username: username,
                pokemon: []
            }]
        }

        const jsonString = JSON.stringify(users)
        
        // Put users array in local storage
        localStorage.setItem('users', jsonString)
        // Change to catalogue page
        this.router.navigate(['catalogue'])
    }
}