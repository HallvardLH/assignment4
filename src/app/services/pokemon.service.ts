import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Pokemon, PokemonData } from "../models/pokemon.model";

// Ensures that this service is a singleton that is shared between all components
@Injectable({
    providedIn: 'root'
})
export class PokemonService{
    // We store the general pokemon data in this object.
    // The results prop in this object contains all the pokemon 
    private _generalData: PokemonData = {} as PokemonData
    private _error: String = ''
    // we store pokemon object in an array. Each object takes in name, url, imageUrl and wether it is collected (clicked) or not
    private _allPokemon: Pokemon[] = []

    constructor(private readonly http: HttpClient){}

    // Get all pokemon from pokeapi
    fetchPokemon(): void{
        // We define what our request is returning
        this.http.get<PokemonData>('https://pokeapi.co/api/v2/pokemon/?limit=10000')
            .subscribe((pokemon: PokemonData) => {
                // Get user's clicked pokemon
                const currentUser = localStorage.getItem('currentUser')
        
                const data = localStorage.getItem('users')
                let users = []
                let clickedPokemon: string[] = []

                if (data !== null) {
                    users = JSON.parse(data)
                    
                    // Get current user and add his clicked pokemon to array
                    for (let i = 0; i < users.length; i++) {
                        if (users[i].username === currentUser) {
                            clickedPokemon = users[i].pokemon
                        }
                    }
                }

                this._generalData = pokemon
                this._allPokemon = this._generalData.results
                // Loop over each pokemon and assign image url
                this._allPokemon.forEach(pokemon => {
                    // Regex for getting number of pokemon
                    const regex = /(?<=\/)\d{1,}(?=\/)/g
                    const index = pokemon.url.match(regex)
                    const baseImgUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index}.png`
                    pokemon.imageUrl = baseImgUrl
                    // Set pokemon as clicked or not which will show pokeball if clicked
                    if (clickedPokemon.indexOf(pokemon.name) > -1) {
                        pokemon.clicked = true
                    }
                    else{
                        pokemon.clicked = false
                    }
                });

            }, (error: HttpErrorResponse) => {
                this._error = error.message
            })
    }

    // Create function to expose private variables to rest of app
    public pokemon(): Pokemon[] {
        return this._allPokemon
    }

    public error(): String {
        return this._error
    }
}