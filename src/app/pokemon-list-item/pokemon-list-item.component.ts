import { Component, Input } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";

@Component({
    selector: 'app-pokemon-list-item',
    templateUrl: './pokemon-list-item.component.html',
    styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent{
    // Input tells a component that it receives information from its parent.
    // So it lets the parent component update data in the child component
    @Input() pokemon: Pokemon | undefined;
    
    // When pokemon is clicked we add it to users list of collected pokemon.
    // We also add a pokeball image next to the pokemon.
    public onPokemonClicked(): void{
        const clickedPokemon = this.pokemon?.name

        // Set pokemon as clicked. This will make a pokeball appear next to it
        if (this.pokemon !== undefined){
            this.pokemon.clicked = true
        }

        const currentUser = localStorage.getItem('currentUser')
        const data = localStorage.getItem('users')
        let users = []

        if (data !== null) {
            users = JSON.parse(data)
            
            // Get current user and add pokemon to list of collected pokemon
            for (let i = 0; i < users.length; i++) {
                if (users[i].username === currentUser) {
                    // Only add if pokemon is not already registered
                    if (users[i].pokemon.indexOf(clickedPokemon) === -1){
                        users[i].pokemon.push(clickedPokemon)
                    }
                }
            }
        }
        // Convert json object to string
        const jsonString = JSON.stringify(users)
        
        // Put updated users array in local storage
        localStorage.setItem('users', jsonString)
    }
}