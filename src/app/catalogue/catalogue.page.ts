import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: 'app-catalogue',
    templateUrl: './catalogue.page.html',
    styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit{
    constructor(private readonly router: Router){}

    ngOnInit(){
        const currentUser = localStorage.getItem('currentUser')
        if (currentUser === null) {
            this.router.navigate(['login'])        
        }
    }
}