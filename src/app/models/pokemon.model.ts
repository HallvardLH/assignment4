export interface Pokemon {
    name: string;
    url: string;
    imageUrl: string;
    clicked: boolean; 
}

export interface PokemonData {
    count: number;
    next: string;
    previous: string;
    results: [];
}