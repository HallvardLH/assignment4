import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Pokemon } from "../models/pokemon.model";
import { PokemonService } from "../services/pokemon.service";

@Component({
    selector: 'app-pokemon-list',
    templateUrl: './pokemon-list.component.html',
    styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit{

    constructor(
        private readonly pokemonService: PokemonService,
        private router: Router
        ) {}
    
    // ngOnInit is called when component is created
    // We fetch pokemon when component is created
    ngOnInit(): void{
        this.pokemonService.fetchPokemon()
    }

    // Change to trainer page
    public goToTrainerPage(){
        this.router.navigate(['trainer'])
    }

    // Change to login page
    public goToLoginPage(){
        this.router.navigate(['login'])
    }

    // Expose variables to the template
    get pokemon(): Pokemon[]{
        return this.pokemonService.pokemon()
    }

}