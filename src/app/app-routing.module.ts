import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CataloguePage } from "./catalogue/catalogue.page";
import { LoginPage } from "./login/login.page";
import { TrainerPage } from "./trainer/trainer.page";

// Each object in the routes represents a route
const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: 'login',
        component: LoginPage,
    },
    {
        path: 'catalogue',
        component: CataloguePage,
    },
    {
        path: 'trainer',
        component: TrainerPage,
    },
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ],
})
export class AppRoutingModule{}